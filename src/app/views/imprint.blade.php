<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Impressum Schönheitsinstitut Panketal</title>
	<meta name="robots" content="noindex">
	<link rel="apple-touch-icon" sizes="57x57" href="images/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/favicons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="images/favicons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="images/favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="images/favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="images/favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="images/favicons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="images/favicons/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="images/favicons/favicon-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="images/favicons/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="images/favicons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="images/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="images/favicons/favicon-32x32.png" sizes="32x32">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="images/favicons/mstile-144x144.png">
	{{ HTML::style('css/app.min.css') }}
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,800' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Oswald:300' rel='stylesheet' type='text/css'>
	{{ HTML::script('js/vendor/modernizr.min.js') }}
	<script type="text/javascript" src="//maps.googleapis.com/maps/api/js"></script>
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
	<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
	<script>
		window.addEventListener("load", function(){
		window.cookieconsent.initialise({
		"palette": {
			"popup": {
			"background": "#000"
			},
			"button": {
			"background": "#f1d600"
			}
		},
		"position": "bottom-right",
		"content": {
			"message": "Diese Website benutzt Cookies. Wenn Sie die Website weiter nutzen, erklären Sie sich damit einverstanden.",
			"dismiss": "Ok",
			"link": "Mehr erfahren",
			"href": "/datenschutz.html"
		}
		})});
	</script>
</head>
<body>
<div class="section head light">
	<div class="row">
		<div class="large-12 columns">
			<a href="/"><img class="logo" src="images/logo.png" alt="Schönheitsinstitut Logo"></a>
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<div class="owl-carousel">
				<img src="images/slider/slide1.jpg" alt="Schönheitsinstitut Einrichtung">
				<img src="images/slider/slide2.jpg" alt="Inh. Ann-Christin Mares ">
				<img src="images/slider/slide3.jpg" alt="Schönheitsinstitut Logo Tür">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<div class="imprint text-center">
				<h1>Schönheitsinstitut</h1>
				<p>
					Inh. Ann-Christin Mares <br> Genfer Platz 2, 16341 Panketal
					<br> Tel: 0 30 64 31 48 92 / Termine nach Vereinbarung </p>
			</div>
		</div>
	</div>
</div>
<div class="section imprint">
	<div class="row">
		<div class="large-12 columns">
			<div class="text-center">
				<h2>Impressum</h2>
			</div>
			<h3>Angaben gemäß § 5 TMG:</h3>

			<p>Ann-Christin Mares <br/> Schönheitsinstitut<br/> Genfer Platz 2<br/> 16341 Panketal OT Schwanebeck
			</p>

			<h3>Kontakt:</h3>

			<p>Telefon: 0 30 64 31 48 92 <br> E-Mail: ann.c@gmx.de</p>

			<h3>Umsatzsteuer-ID:</h3>

			<p>Umsatzsteuer-Identifikationsnummer gemäß §27 a Umsatzsteuergesetz:<br/> 065/291/00742 
			</p>

			<h3>Haftungsausschluss (Disclaimer)</h3>

			<p><strong>Haftung für Inhalte</strong></p>

			<p>Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.</p>

			<p><strong>Haftung für Links</strong></p>

			<p>Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.</p>

			<p><strong>Urheberrecht</strong></p>

			<p>Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet. Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.</p>

			<p>Quelle: http://www.e-recht24.de </p>
		</div>
	</div>
</div>
<footer>
	<div class="row">
		<div class="large-12 columns text-center">
			Schönheitsinstitut <?php echo date('Y', time()); ?> | <a href="/impressum">Impressum</a>| <a href="/datenschutz">Datenschutz</a>
		</div>
	</div>
</footer>
{{ HTML::script('js/vendor/libraries.min.js') }} {{ HTML::script('js/vendor/foundation.min.js') }} {{ HTML::script('js/app.min.js') }}
</body>
</html>