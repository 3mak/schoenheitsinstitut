<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Schönheitsinstitut</title>
	<link rel="apple-touch-icon" sizes="57x57" href="images/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/favicons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="images/favicons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="images/favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="images/favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="images/favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="images/favicons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="images/favicons/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="images/favicons/favicon-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="images/favicons/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="images/favicons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="images/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="images/favicons/favicon-32x32.png" sizes="32x32">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="images/favicons/mstile-144x144.png">
	{{ HTML::style('css/app.min.css') }}
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,800' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Oswald:300' rel='stylesheet' type='text/css'>
	{{ HTML::script('js/vendor/modernizr.min.js') }}
	<script type="text/javascript" src="//maps.googleapis.com/maps/api/js"></script>
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
	<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
	<script>
		window.addEventListener("load", function(){
		window.cookieconsent.initialise({
		"palette": {
			"popup": {
			"background": "#000"
			},
			"button": {
			"background": "#f1d600"
			}
		},
		"position": "bottom-right",
		"content": {
			"message": "Diese Website benutzt Cookies. Wenn Sie die Website weiter nutzen, erklären Sie sich damit einverstanden.",
			"dismiss": "Ok",
			"link": "Mehr erfahren",
			"href": "/datenschutz.html"
		}
		})});
	</script>
</head>
<body>
<div class="section head light">
	<div class="row">
		<div class="large-12 columns">
			<a href="/"><img class="logo" src="images/logo.png" alt="Schönheitsinstitut Logo"></a>
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<div class="owl-carousel">
				<img src="images/slider/slide1.jpg" alt="Schönheitsinstitut Einrichtung">
				<img src="images/slider/slide2.jpg" alt="Inh. Ann-Christin Mares ">
				<img src="images/slider/slide3.jpg" alt="Schönheitsinstitut Logo Tür">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<div class="imprint text-center">
				<h1>Schönheitsinstitut</h1>

				<p>
					Inh. Ann-Christin Mares  <br> Genfer Platz 2, 16341 Panketal
					<br> Tel: 0 30 64 31 48 92 / Termine nach Vereinbarung </p>
			</div>
		</div>
	</div>
</div>
<div class="section contact">
	<div class="row">
		<div class="large-12 columns">
			<h1>Ihre Nachricht wurde versandt.</h1>
			<p>Vielen Dank für ihre Nachricht, wir werden uns so schnell wie möglich bei ihnen melden.</p>
		</div>
	</div>
</div>
<footer>
	<div class="row">
		<div class="large-12 columns text-center">
			Schönheitsinstitut <?php echo date('Y', time()); ?> | <a href="/impressum">Impressum</a>| <a href="/datenschutz">Datenschutz</a>
		</div>
	</div>
</footer>
{{ HTML::script('js/vendor/libraries.min.js') }} {{ HTML::script('js/vendor/foundation.min.js') }} {{ HTML::script('js/app.min.js') }}
</body>
</html>