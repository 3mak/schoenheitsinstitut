<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Datenschutz Schönheitsinstitut</title>
	<meta name="robots" content="noindex">
	<link rel="apple-touch-icon" sizes="57x57" href="images/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/favicons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="images/favicons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="images/favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="images/favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="images/favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="images/favicons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="images/favicons/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="images/favicons/favicon-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="images/favicons/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="images/favicons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="images/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="images/favicons/favicon-32x32.png" sizes="32x32">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="images/favicons/mstile-144x144.png">
	{{ HTML::style('css/app.min.css') }}
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,800' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Oswald:300' rel='stylesheet' type='text/css'>
	{{ HTML::script('js/vendor/modernizr.min.js') }}
	<script type="text/javascript" src="//maps.googleapis.com/maps/api/js"></script>
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
	<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
	<script>
		window.addEventListener("load", function(){
		window.cookieconsent.initialise({
		"palette": {
			"popup": {
			"background": "#000"
			},
			"button": {
			"background": "#f1d600"
			}
		},
		"position": "bottom-right",
		"content": {
			"message": "Diese Website benutzt Cookies. Wenn Sie die Website weiter nutzen, erklären Sie sich damit einverstanden.",
			"dismiss": "Ok",
			"link": "Mehr erfahren",
			"href": "/datenschutz.html"
		}
		})});
	</script>
</head>
<body>
<div class="section head light">
	<div class="row">
		<div class="large-12 columns">
			<a href="/"><img class="logo" src="images/logo.png" alt="Schönheitsinstitut Logo"></a>
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<div class="owl-carousel">
				<img src="images/slider/slide1.jpg" alt="Schönheitsinstitut Einrichtung">
				<img src="images/slider/slide2.jpg" alt="Inh. Ann-Christin Mares ">
				<img src="images/slider/slide3.jpg" alt="Schönheitsinstitut Logo Tür">
			</div>
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<div class="imprint text-center">
				<h1>Schönheitsinstitut</h1>
				<p>
					Inh. Ann-Christin Mares  <br> Genfer Platz 2, 16341 Panketal
					<br> Tel: 0 30 64 31 48 92 / Termine nach Vereinbarung </p>
			</div>
		</div>
	</div>
</div>
<div class="section imprint">
	<div class="row">
		<div class="large-12 columns">
			<div class="text-center">
				<h2>Datenschutz</h2>
			</div>
			<p><strong>Datenschutz</strong></p>
			<p>Die Nutzung unserer Webseite ist in der Regel ohne Angabe personenbezogener Daten möglich. Soweit auf unseren Seiten personenbezogene Daten (beispielsweise Name, Anschrift oder eMail-Adressen) erhoben werden, erfolgt dies, soweit möglich, stets auf freiwilliger Basis. Diese Daten werden ohne Ihre ausdrückliche Zustimmung nicht an Dritte weitergegeben. </p> <p>Wir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich. </p> <p>Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten durch Dritte zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit ausdrücklich widersprochen. Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-Mails, vor.
			</p>
			<p><strong>Datenschutzerklärung für die Nutzung von Google Analytics</strong></p>
			<p>Diese Website benutzt Google Analytics, einen Webanalysedienst der Google Inc. ("Google"). Google Analytics verwendet sog. "Cookies", Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch den Cookie erzeugten Informationen über Ihre Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert. Im Falle der Aktivierung der IP-Anonymisierung auf dieser Webseite wird Ihre IP-Adresse von Google jedoch innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt.</p> <p>Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt.</p> <p>Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem sie das unter dem folgenden Link verfügbare Browser-Plugin herunterladen und installieren: <a href="http://tools.google.com/dlpage/gaoptout?hl=de">http://tools.google.com/dlpage/gaoptout?hl=de</a>.<p> </p>
		</div>
	</div>
</div>
<footer>
	<div class="row">
		<div class="large-12 columns text-center">
			Schönheitsinstitut <?php echo date('Y', time()); ?> | <a href="/impressum">Impressum</a>| <a href="/datenschutz">Datenschutz</a>
		</div>
	</div>
</footer>
{{ HTML::script('js/vendor/libraries.min.js') }} {{ HTML::script('js/vendor/foundation.min.js') }} {{ HTML::script('js/app.min.js') }}
</body>
</html>