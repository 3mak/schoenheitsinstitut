<?php

class ContactController extends BaseController {
	
	public function submit() {

		$data = Input::all();

		$rules = array(
			'name' => 'required',
			'email' => 'required|email',
			'message' => 'required|min:5',
			'gpdr' => 'required'
		);

		$messages = array(
			'name.required'    => 'Bitte geben Sie einen Namen an.',
			'email.required'   => 'Gitte geben Sie ihre Email-Adresse an.',
			'message.required' => 'Sie haben vergessen ihre Nachricht zu schreiben.',
			'email'            => 'Bitte geben sie eine Korrekte Email-Adresse an.',
			'message.min'      => 'Bitte schreibe sie eine ausführlichere Nachrricht.',
			'gpdr.required'	   => 'Sie müssen in die Datenschutzerklärung einwilligen.'
		);

		$validator = Validator::make($data, $rules, $messages);


		if($validator->passes()) {
			Mail::send('emails.contact', $data, function($message) use ($data) {
				$message->from($data['email'], $data['name']);
				$message->to('info@schoenheitsinstitut-panketal.de', 'Schönheitsinstitut')->subject('Kontaktformular');
			});

			return View::make('thank-you');
		} else {
			return Redirect::to('/#kontakt')->withErrors($validator);
		}
	}
}