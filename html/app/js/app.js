/*global google */

var app = (function(document, $, google) {

	'use strict';
	var docElem = document.documentElement,

		_userAgentInit = function() {
			docElem.setAttribute('data-useragent', navigator.userAgent);
		},
		/**
		* Init owl Carousel for Images
		* on top of the site.
		*/
		_initHeadOwlCarousel = function() {
			$('.owl-carousel').owlCarousel({
				items: 1,
				dots: true,
				autoplay:true
			});
		},
		/**
		* Init Owl Carousel for price list
		*
		* Int Carousel and fire trogger on arrow click,
		*/ 
		_initPriceCarousel = function() {
			var owl = $('.owl-carousel-price').owlCarousel({
				margin: 50,
				dots: false,
				//autoHeight : true,
				responsive: {
			        0:{
			            items:1
			        },
			        '1024':{
			            items:2
			        }
				}
			});

			$('.right-arrow').click(function(e) {
				e.preventDefault();
			    owl.trigger('next.owl.carousel');
			});

			$('.left-arrow').click(function(e) {
				e.preventDefault();
			    owl.trigger('prev.owl.carousel');
			});
		},
		/**
		*  Init Google Maps via JS Api
		* 
		*/		
		_initGoogleMapsApi = function() {
			var canvas = document.getElementById('map_canvas');

			var canvasWidth = $(canvas).css('width');
			$(canvas).css('height', canvasWidth);	

	        var mapOptions = {
	          center: new google.maps.LatLng(52.638544, 13.526080),
	          zoom: 16,
	          scrollwheel: false,
	          mapTypeId: google.maps.MapTypeId.ROADMAP
	        };
	        var map = new google.maps.Map(document.getElementById('map_canvas'),
	            mapOptions);

	        new google.maps.Marker({
				position: new google.maps.LatLng(52.638544, 13.526080),
				map: map,
				title:'Schönheitsinstitut'
  			});
      	},
      	/**
      	* Call all init scripts. 
      	* 
      	*/
		_init = function() {
			_userAgentInit();
			_initHeadOwlCarousel();
			_initPriceCarousel();
			_initGoogleMapsApi();
		};

	return {
		init: _init
	};

})(document, jQuery, google);

(function() {

	'use strict';
	app.init();

})();