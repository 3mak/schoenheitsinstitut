<!doctype html>
<html class="no-js" lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Schönheitsinstitut Panketal - Ihr Beautysalon</title>
	<meta name="description" content="Das Schönheitsinstitut, ihr Beautysalon in Panketal,  ist ihr Spezialist für Kosmetik, Maniküre, Fußpflege und Handbehandungen.">
	<link rel="apple-touch-icon" sizes="57x57" href="images/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/favicons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="images/favicons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="images/favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="images/favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="images/favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="images/favicons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="images/favicons/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="images/favicons/favicon-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="images/favicons/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="images/favicons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="images/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="images/favicons/favicon-32x32.png" sizes="32x32">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="images/favicons/mstile-144x144.png">

	{{ HTML::style('css/app.min.css') }}
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,800' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Oswald:300' rel='stylesheet' type='text/css'>
	{{ HTML::script('js/vendor/modernizr.min.js') }}
	<script type="text/javascript" src="//maps.googleapis.com/maps/api/js"></script>
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
	<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
	<script>
		window.addEventListener("load", function () {
			window.cookieconsent.initialise({
				"palette": {
					"popup": {
						"background": "#000"
					},
					"button": {
						"background": "#f1d600"
					}
				},
				"position": "bottom-right",
				"content": {
					"message": "Diese Website benutzt Cookies. Wenn Sie die Website weiter nutzen, erklären Sie sich damit einverstanden.",
					"dismiss": "Ok",
					"link": "Mehr erfahren",
					"href": "/datenschutz.html"
				}
			})
		});
	</script>
</head>

<body>
	<div class="section head light">
		<div class="row">
			<div class="large-12 columns">
				<img class="logo" src="images/logo.png" alt="Schönheitsinstitut Logo">
			</div>
		</div>
		<div class="row">
			<div class="large-12 columns">
				<div class="owl-carousel">
					<img src="images/slider/slide1.jpg" alt="Schönheitsinstitut Einrichtung">
					<img src="images/slider/slide2.jpg" alt="Schönheitsinstitut Innenansicht">
					<img src="images/slider/slide3.jpg" alt="Schönheitsinstitut Logo Tür">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="large-12 columns">
				<div class="imprint text-center">
					<h1>Schönheitsinstitut</h1>

					<p>
						Inh. Ann-Christin Mares <br> Genfer Platz 2, 16341 Panketal
						<br> Tel: 0 30 64 31 48 92 / Termine nach Vereinbarung </p>
				</div>
			</div>
		</div>
	</div>
	<section id="about-me">
		<div class="section about-me">
			<div class="row">
				<div class="large-12 columns">
					<div class="text-center subhead">
						<img src="images/ann-christin.png" alt="Ann Christin" />

						<h2>Über mich</h2>
					</div>
					<p>Ich wuchs in Graal-Müritz an der Ostsee auf. Nach meiner Ausbildung zur Einzelhandelskauffrau, studierte ich
						vier Semester Betriebswirtschaftslehre.</p>

					<p>Meine berufliche Bestimmung sah ich aber in ganz anderen Bereichen und so begann ich die Ausbildung zur
						"staatlich anerkannten Kosmetikerin" und schloss diese mit Auszeichnung ab. Seit 2003 arbeite ich als
						Kosmetikerin, Fußpflegerin und Nagelstylistin und habe darin meine absolute Erfüllung gefunden! Vorerst arbeitete
						ich in einer Hautarztpraxis, dann im KaDeWe für die Firma "alessandro international" und seit 2011 in einem
						"alessandro-Institut" in Berlin.</p>

					<p>Seit 2012 lebe ich mit meinem Partner und unseren drei Kindern in Schwanebeck und habe mir hier im März 2014 meinen Traum von meinem „Schönheitsinstitut“ erfüllt.</p>
				</div>
			</div>
		</div>
	</section>
	<section id="preise">
		<div class="section price">
			<div class="row">
				<div class="large-12 columns">
					<div class="text-center">
						<h2>Preisübersicht</h2>
					</div>


					<div class="carousel-container">
						<a class="left-arrow" href="#">&nbsp;</a>

						<div class="center">
							<div class="owl-carousel-price">
								<div>
									<h3>Kosmetik</h3>
									<table>
										<tr>
											<td>
												<h4>Kosmetikbehandlung</h4>
												<p>(Reinigung, Peeling, Tiefenreinigung, Brauen formen, Brauen und Wimpern färben, Wirkstoffkonzentrat,
													Maske, Gesichtsmassge)</p>
											</td>

											<td>ab 43,00 €</td>
										</tr>
										<tr>
											<td>
												<h4>Express Kosmetikbehandlung</h4>
												<p>(Reinigung, Peeling, Tiefenreinigung, Wirkstoffkonzentrat, Maske)</p>
											</td>
											<td>ab 32,00 €</td>
										</tr>
									</table>
								</div>


								<div>
									<h3>Dermazeutische Behandlungen</h3>


									<table>
										<tr>
											<td>
												<h4>Ultraschallbehandlung</h4>
												<p>(Reinigung, enzymatisches Peeling, Tiefenreinigung, Ultraschallbehandlung, mit Wirkstoffkonzentrat,
													Maske, Gesichtsmassage, Tagespflege)</p>
												<ul>
													<li>durch stimulierenden Effekt der Ultraschallwellen wird der Zellstoffwechsel beschleunigt</li>
													<li>Reduktion von Augenringen und Tränensäcken</li>
													<li>intensivere Wirkstoffeinschleusung</li>
												</ul>
											</td>
											<td>69,00 €</td>
										</tr>
									</table>
								</div>


								<div>
									<table>
										<tr>
											<td>
												<h4>DiamondPeel mit Ultraschallbehandlung</h4>


												<p>(Reinigung, enzymatisches Peeling, Diamant Mikrodermabrasion, Tiefenreinigung, Ultraschallbehandlung mit
													Wirkstoffkonzentrat, Maske, Gesichtsmassage, Tagespflege)</p>


												<ul>
													<li>sofortiger "Lift-Effekt"</li>
													<li>durch stimulierenden Effekt der Ultraschallwellen wird der Zellstoffwechsel beschleunigt</li>
													<li>Reduktion von Augenringen und Tränensäcken</li>
													<li>intensivere Wirkstoffeinschleusung</li>
													<li>Stimulierung von Kollagen und Elastin</li>
													<li>Verbesserung des Hautzustandes</li>
												</ul>
											</td>

											<td>89,00 €</td>
										</tr>

										<tr>
											<td>
												<h4>Augenbrauen zupfen/waxen</h4>
											</td>
											<td>5,00 €</td>
										</tr>
										<tr>
											<td>
												<h4>Augenbrauen färben</h4>
											</td>
											<td>6,00 €</td>
										</tr>
										<tr>
											<td>
												<h4>Wimpern färben</h4>
											</td>
											<td>7,00 €</td>
										</tr>
									</table>
								</div>


								<div>
									<h3>Fußpflege</h3>


									<table>
										<tr>
											<td>
												<h4>Klassische Fußpflege</h4>


												<p>(Fußbad, Nägel formen und reinigen, Hornhaut entfernen)</p>
											</td>

											<td>21,00 €</td>
										</tr>

										<tr>
											<td>
												<h4>French oder Farbgel</h4>


												<p>(in der Fußpflegebehandlung)</p>
											</td>

											<td>11,00 €</td>
										</tr>


										<tr>
											<td>
												<h4>Nagellack</h4>


												<p>(in der Fußpflegebehandlung)</p>
											</td>

											<td>6,00 €</td>
										</tr>
									</table>
								</div>


								<div>
									<h3>Handbehandlungen</h3>


									<table>
										<tr>
											<td>
												<h4>Klassische Maniküre</h4>


												<p>(Nägel kürzen und formen, Nagelhaut entfernen)</p>
											</td>

											<td>18,00 €</td>
										</tr>


										<tr>
											<td>
												<h4>Nagellack</h4>


												<p>(in der Maniküre)</p>
											</td>

											<td>6,00 €</td>
										</tr>
										<tr>
											<td>
												<h4>Naturnagelverstärkung</h4>


												<p>(Neu oder auffüllen nach 3-4 Wochen ink. 1 Farbe oder French)</p>
											</td>

											<td>ab 37,00 €</td>
										</tr>

										<tr>
											<td>
												<h4>Nagelmodelage</h4>
												<p>(Neu oder auffüllen nach 3-4 Wochen ink. 1 Farbe oder French)</p>
											</td>

											<td>ab 43,00 €</td>
										</tr>

										<tr>
											<td>
												<h4>jede weitere Farbe, Strass und Nailart pro Nagel</h4>
											</td>

											<td>je 0,50 €</td>
										</tr>
									</table>
								</div>


								<div>
									<h3>Enthaarung</h3>


									<table>
										<tr>
											<td>
												<h4>Achseln</h4>
												<p></p>
											</td>

											<td>9,00 €</td>
										</tr>


										<tr>
											<td>
												<h4>Brust, Bauch, Rücken, Bikinizone</h4>
												<p></p>
											</td>

											<td>je 19,00 €</td>
										</tr>

										<tr>
											<td>
												<h4>(beide) Oberschenkel, (beide) Unterschenkel</h4>
												<p></p>
											</td>

											<td>je 19,00 €</td>
										</tr>
									</table>
									
									<h3>Wimpernverlängerung</h3>
									<table>
										<tr>
											<td><h4>Wimpernverlängerung</h4></td>
											<td>99,00 €</td>
										</tr>
										<tr>
											<td><h4>Auffüllen der Wimpernverlängerung</h4></td>
											<td>ab 45,00 €</td>
										</tr>
									</table>
								</div>

								<div>
									<h3>Permanent-Make-Up</h3>
									<table>
										<tr>
											<td><h4>Wimpernkranzverdichtung oben oder unten</h4></td>
											<td>je 100 €</td>
										</tr>
										<tr>
											<td><h4>Lidstrich</h4></td>
											<td>160 €</td>
										</tr>
										<tr>
											<td><h4>Augenbrauen Härchenzeichnung</h4></td>
											<td>200 €</td>
										</tr>
										<tr>
											<td><h4>Augenbrauen Vollschattierung</h4></td>
											<td>300 €</td>
										</tr>
										<tr>
											<td><h4>Lippen Kontur</h4></td>
											<td>200 €</td>
										</tr>
										<tr>
											<td><h4>Lippen Anschattierung</h4></td>
											<td>250 €</td>
										</tr>
										<tr>
											<td><h4>Lippen Vollschattierung</h4></td>
											<td>400 €</td>
										</tr>
									</table>
									<br>
									<p>In allen Permanent-Make-Up-Preisen ist jeweils eine Nachbehandlung innerhalb von 2 Monaten inklusive.</p>
									<p>Eine Nachbehandlung nach mehr als 1 Jahr kostet die Hälfte des o.g. Preises.</p>
								</div>

							</div>
						</div>
						<a class="right-arrow" href="#">&nbsp;</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="kontakt">
		<div class="contact section">
			<div class="row">
				<div class="medium-6 columns">
					<div class="text-right">
						<h2>Kontakt</h2>
					</div>
					<p>
						<strong> Schönheitsinstitut <br> Genfer Platz 2, 16341 Panketal
							<br> Tel: 0 30 64 31 48 92 </strong>
					</p>
				</div>
			</div>
			<div class="row">
				<div class="medium-6 columns">
					<ul class="errors">
						@foreach($errors->all('
						<li>:message</li>
						') as $message) {{ $message }} @endforeach
					</ul>
					{{ Form::open(array('url' => 'contact')) }}
					{{ Form::text ('name', '', array('placeholder' => 'Name')) }}
					{{ Form::email ('email', '', array('placeholder' => 'E-Mail')) }}
					{{ Form::textarea ('message', '', array('placeholder' => 'Nachricht')) }}
					<fieldset class="form__gpdr">
						{{ Form::checkbox('gpdr',null, null, array('placeholder' => 'gpdr')) }}
						<label for="gdpr">Ich stimme zu, dass meine Angaben aus dem Kontaktformular zur Beantwortung meiner Anfrage
							erhoben und verarbeitet werden. Detaillierte Informationen zum Umgang mit Nutzerdaten finden Sie in unserer <a
							 href="/datenschutz.html">Datenschutzerklärung</a>.</label>
					</fieldset>
					{{ Form::submit('Senden', array('class' => 'btn right')) }}

					{{ Form:: close() }}
				</div>
				<div class="medium-6 columns">
					<div id="map_canvas"></div>
				</div>
			</div>
		</div>
	</section>
	<footer>
		<div class="row">
			<div class="large-12 columns text-center">
				Schönheitsinstitut
				<?php echo date('Y', time()); ?> |
				<a href="/impressum">Impressum</a>|
				<a href="/datenschutz">Datenschutz</a>
			</div>
		</div>
	</footer>

	{{ HTML::script('js/vendor/libraries.min.js') }} {{ HTML::script('js/vendor/foundation.min.js') }} {{
	HTML::script('js/app.min.js') }}
</body>

</html>