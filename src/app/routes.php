<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('index');
});

Route::post('contact', 'ContactController@submit');

Route::get('impressum', function()
{
	return View::make('imprint');
});

Route::get('datenschutz', function()
{
	return View::make('data-privacy');
});
